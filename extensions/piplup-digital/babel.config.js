const merge = require('babel-merge');

const configGetter = require('@mindtickle/frontend-configs/lib/babel');
module.exports = function (api) {
  const config = configGetter(api, { corejs: 2 });

  return merge(config, {
    presets: ['@babel/preset-typescript'],
    plugins: [
      [
        'import',
        {
          libraryName: 'antd',
          style: true,
        },
      ],
      [
        'react-intl',
        {
          overrideIdFn: id => id.replace('__tmp__.', ''),
        },
      ],
      [
        'react-intl-auto',
        {
          relativeTo: '..',
        },
      ],
    ],
    env: {
      test: {
        plugins: ['react-intl-auto'],
      },
    },
  });
};
