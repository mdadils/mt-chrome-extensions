import * as InboxSDK from '@inboxsdk/core';

import Modal from '@mindtickle/modal';
import TestApp from './TestApp';

InboxSDK.load(2, 'sdk_asset-picker_4285954f77', {}).then(sdk => {
  sdk.Compose.registerComposeViewHandler(composeView => {
    composeView.addButton({
      title: 'MindTickle',
      iconUrl:
        'https://qualified-production.s3.us-east-1.amazonaws.com/uploads/50d62012c70d2c11f3152fa4624c9e0bd3ba6492d624773adb5ec95ec7531d15.png',
      onClick(event) {
        Modal.confirm({
          fallback: <div>This is a fallback</div>,
          content: <TestApp />,
        });
      },
    });
  });
});
