require('dotenv').config();
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

const merge = require('webpack-merge');
const webpackConfig = require('@mindtickle/frontend-configs/lib/webpack');

const { directoryMap, alias, pathsToTranspile } = require('./config/constants');
const { federationPluginOptions } = require('./plugins');

const antTheme = require('@mindtickle/styles/cjs/antTheme').default;

const config = webpackConfig({
  directoryMap,
  webpackConfig: {
    alias,
    federationPluginOptions,
    pathsToTranspile,
  },
});

const finalConfig = merge(config, {
  entry: {
    contentScript: path.join(__dirname, '..', 'src', 'index.ts'),
  },
  devtool: false,
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false,
      }),
    ],
  },
  output: {
    clean: true,
    asyncChunks: false,
    filename: '[name].bundle.js',
  },
  resolve: {
    symlinks: false,
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'less-loader',
            options: {
              lessOptions: {
                modifyVars: antTheme,
                javascriptEnabled: true,
              },
            },
          },
        ],
      },
    ],
  },
});

delete finalConfig.optimization.splitChunks;

module.exports = finalConfig;
