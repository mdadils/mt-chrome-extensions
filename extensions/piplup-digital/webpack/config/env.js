const getenv = require('getenv');

const config = getenv.multi({
  NODE_ENV: ['NODE_ENV', 'development', 'string'],
  FEDERATION_REMOTE_FILE_NAME: ['FEDERATION_REMOTE_FILE_NAME', 'remoteEntry', 'string'],
  FEDERATION_HOST_SHELL: ['FEDERATION_HOST_SHELL', '', 'string'],
});

module.exports = config;
