const path = require('path');

const { DIRECTORY_KEYS } = require('@mindtickle/frontend-configs/lib/webpack/constants');

const directoryMap = {
  [DIRECTORY_KEYS.ENTRY_POINT]: path.resolve(__dirname, '..', '..', 'src', 'index'),
  [DIRECTORY_KEYS.OUTPUT]: path.resolve(__dirname, '..', '..', 'dist'),
  [DIRECTORY_KEYS.APP_BASE]: path.resolve(__dirname, '..', '..'),
  [DIRECTORY_KEYS.APP_SRC]: path.resolve(__dirname, '..', '..', 'src'),
  [DIRECTORY_KEYS.DOT_ENV_FILE]: path.resolve(__dirname, '..', '..', '.env'),
  [DIRECTORY_KEYS.BABEL_CONFIG_FILE]: path.resolve(__dirname, '..', '..', 'babel.config.js'),
  [DIRECTORY_KEYS.ESLINT_CONFIG]: path.resolve(__dirname, '..', '..', '.eslintrc'),
};

const alias = {
  '~': `${directoryMap[DIRECTORY_KEYS.APP_SRC]}`,
};

const pathsToTranspile = [];

module.exports = {
  directoryMap,
  alias,
  pathsToTranspile,
};
