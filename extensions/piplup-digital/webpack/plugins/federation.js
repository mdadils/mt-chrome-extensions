const pkg = require('../../package.json');
const ENV = require('../config/env');

const remote_file = `${ENV.FEDERATION_REMOTE_FILE_NAME}.js`;

module.exports = {
  name: pkg.name.split('-').join('_'),
  filename: remote_file,
  exposes: {},
  remotes: {},
  shared: [
    {
      ...pkg.dependencies,
      'react-router-dom': {
        singleton: true,
        requiredVersion: pkg.dependencies['react-router-dom'],
      },
      react: {
        singleton: true,
        requiredVersion: pkg.dependencies.react,
      },
      'react-dom': {
        singleton: true,
        requiredVersion: pkg.dependencies['react-dom'],
      },
    },
  ],
};
