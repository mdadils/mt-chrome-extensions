module.exports = {
  extends: ['@mindtickle/frontend-configs/src/commitlint'],
  rules: {
    'type-enum': [2, 'always', ['feat', 'fix', 'ui', 'refactor', 'perf', 'test', 'docs', 'wip']],
  },
};
