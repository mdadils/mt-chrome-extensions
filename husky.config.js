module.exports = {
  hooks: {
    'commit-msg': '[[ -n $HUSKY_BYPASS ]] || commitlint -E HUSKY_GIT_PARAMS',
    'pre-commit': 'lint-staged',
    'post-merge': "echo 'post merge/pull reminder: consider running yarn install'",
  },
};
